const db = require('../database');
const { Sequelize } = require('sequelize');

const Video = db.define('video',{
    title: {
        type: Sequelize.STRING
    },
    type: {
        type: Sequelize.ENUM('video_path', 'url'),
        defaultValue:"url",
        allowNull: true
    },
    description: {
        allowNull: true,
        type: Sequelize.TEXT
    },
    url: {
        allowNull: true,
        type: Sequelize.STRING
    },
    path: {
        allowNull: true,
        type: Sequelize.STRING
    },
    active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },
});

module.exports = Video;