const db = require('../database');
const { Sequelize } = require('sequelize');

const aboutUs = db.define('about_us',{
    descriptionHtml: {
      type: Sequelize.TEXT
    },
    createdAt: {
      type: Sequelize.DATE
    },
    updatedAt: {
      type: Sequelize.DATE
    }
});

module.exports = aboutUs;