const db = require('../database');
const { Sequelize } = require('sequelize');

const ServiceType = db.define('service_types',{
    service_name: {
        type: Sequelize.STRING
    },
    active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },
    description: {
        type: Sequelize.TEXT
    },
    descriptionHtml: {
        type: Sequelize.TEXT
    },
});

module.exports = ServiceType;