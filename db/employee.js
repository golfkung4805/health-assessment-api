const db = require('../database');
const { Sequelize } = require('sequelize');

const Employee = db.define('employees',{
    code: {
        type: Sequelize.STRING
    },
    fullname: {
        type: Sequelize.STRING,
        defaultValue: true
    },
    nickname: {
        type: Sequelize.STRING
    },
    birthday: {
        type: Sequelize.DATE
    },
    tel: {
        type: Sequelize.STRING
    },
    address: {
        type: Sequelize.TEXT
    },
    license: {
        type: Sequelize.STRING
    },
    licenseImg: {
        type: Sequelize.TEXT
    },
    course1: {
        type: Sequelize.FLOAT
    },
    course2: {
        type: Sequelize.FLOAT
    },
    course3: {
        type: Sequelize.FLOAT
    },
    premium_course: {
        type: Sequelize.FLOAT
    },
    description: {
        type: Sequelize.TEXT
    },
    descriptionHtml: {
        type: Sequelize.TEXT
    },
    active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
});

module.exports = Employee;