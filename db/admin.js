const db = require('../database');
const { Sequelize } = require('sequelize');

const Admin = db.define('admin',{
    name: {
      type: Sequelize.STRING
    },
    username: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    role: {
      type: Sequelize.STRING
    },
    token: {
      type: Sequelize.TEXT
    }
});

module.exports = Admin;