'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      code: {
        allowNull: true,
        type: Sequelize.STRING
      },
      fullname: {
        allowNull: false,
        type: Sequelize.STRING
      },
      nickname: {
        allowNull: false,
        type: Sequelize.STRING
      },
      birthday: {
        allowNull: false,
        type: Sequelize.DATE
      },
      tel: {
        allowNull: false,
        type: Sequelize.STRING
      },
      address: {
        allowNull: true,
        type: Sequelize.STRING
      },
      license: {
        allowNull: true,
        type: Sequelize.STRING
      },
      licenseImg: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      course1: {
        allowNull: true,
        type: Sequelize.FLOAT 
      },
      course2: {
        allowNull: true,
        type: Sequelize.FLOAT 
      },
      course3: {
        allowNull: true,
        type: Sequelize.FLOAT 
      },
      premium_course: {
        allowNull: true,
        type: Sequelize.FLOAT 
      },
      descriptionHtml: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      description: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('employee');
  }
};
