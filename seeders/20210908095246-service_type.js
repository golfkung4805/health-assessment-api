'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('service_types', [
      {
        service_name: 'ทีมพยาบาล',
        active: true,
        description: null,
        descriptionHtml: null
      },
      {
        service_name: 'ผู้ช่วยพยาบาล',
        active: true,
        description: null,
        descriptionHtml: null
      },
      {
        service_name: 'ผู้ดูแล',
        active: true,
        description: null,
        descriptionHtml: null
      }
  ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('service_types', null, {});
  }
};
