'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.bulkInsert('admins', [{
        name: 'Super Admin',
        username: 'super_admin',
        password: '$2a$10$CD.wdQ.VwVPWxJO7DCOlneTw2vyFQrtH/scKnD1DgdChHqFrPtbNa',
        role:'super_admin'
     }], {});
  },

  down: async (queryInterface, Sequelize) => {
     await queryInterface.bulkDelete('admins', null, {});
  }
};
