const jwt = require('jsonwebtoken');
const Admin = require('../db/admin');

module.exports = async (req, res, next) => {
  try {
    var authorization = req.session.authorization || req.headers.authorization
    var token = authorization.replace('Bearer ', '')
    req.decode = jwt.verify(token, 'health-assessment')
    next()
  } catch (error) {
    console.log(error)
    if (error.name === 'TokenExpiredError') {
      await Admin.update({ token }, { where: { token: '' } })
      res.status(401).json({ message: 'Token expired' })
    } else if (error.name === 'JsonWebTokenError') {
      res.status(401).json({ message: 'Token invalid' })
    } else {
      res.status(404).json({ message: 'Token not found' })
    }
  }
}
