
const bcrypt = require('bcryptjs');

exports.create = (model) => {
  return async (req, res, next) => {
    try {
        const data = await model.create({
          ...req.body,
          active: true
        });
        req.result = { data }
      next()
    } catch (error) {
      console.log(error);
      res.status(400).json({ status: false, message: 'data invalid' })
    }
  }
}

exports.getAll = (model) => {
    return async (req, res, next) => {
      try {
        var data = await model.findAll();
        req.result = { data }
        next()
      } catch (error) {
        console.log(error)
        res.status(400).json({ status: false, message: 'data invalid' })
      }
    }
}

exports.getById = (model) => {
    return async (req, res, next) => {
      try {
        var data = await model.findOne({ where: { id: req.params.id }});
        req.result = { data }
        next()
      } catch (error) {
        console.log(error)
        res.status(400).json({ status: false, message: 'data invalid' })
      }
    }
}

exports.edit = (model) => {
    return async (req, res, next) => {
      try {
        const data = await model.update(
            { ...req.body, updatedAt : new Date() },
            { where: { id: parseInt(req.params.id) } }
        );
        req.result = { data }
        next()
      } catch (error) {
        console.log(error);
        res.status(400).json({ status: false,  message: 'data invalid' })
      }
    }
}

exports.del = (model) => {
    return async (req, res, next) => {
      try {
        var data = await model.destroy({ where: { id: req.params.id }});
        req.result = { data }
        next()
      } catch (error) {
        console.log(error)
        res.status(400).json({ status: false, message: 'data invalid' })
      }
    }
}