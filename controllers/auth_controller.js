const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Admin = require("../db/admin");

exports.login = () => {
  return async (req, res, next) => {
    try {
      const admin = await Admin.findOne({ where: { username: req.body.username }});
      if (admin && bcrypt.compareSync(req.body.password, admin['password'])) {
        const token = jwt.sign(
          {
            name: admin['name'],
            username: admin['username'],
            role: admin['role'],
          },
          "health-assessment",
          { expiresIn: 8 * 60 * 60 }
        );
        const accessToken = await Admin.update(
          { token: token },
          { where: { id: admin['id'] } }
        );
        if (accessToken.nModified) console.log("token updated");
        req.session.authorization = `Bearer ${token}`;
        req.result = {
          id: admin.id,
          name: admin.name,
          username: admin.username,
          role: admin.role,
          access_token: token,
          createdAt: admin.createdAt,
          updatedAt: admin.updatedAt
        };
        next();
      } else {
        res.status(401).json({ status: false, message: "Username or Password invalid" });
      }
    } catch (error) {
      console.log(error);
      res
        .status(400)
        .json({ status: false, message: "admin login data invalid" });
    }
  };
};
