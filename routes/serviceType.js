const express = require('express');
const router = express.Router();

const ServiceType = require("../db/serviceType");

const validate = require('../controllers/validate');
const serviceTypeUtil = require('../controllers/serviceType_controller');

router.post('/', serviceTypeUtil.create(ServiceType), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create service type success' })
});

router.get('/', serviceTypeUtil.getAll(ServiceType), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create service type success', result: req.result })
});

router.get('/:id', serviceTypeUtil.getById(ServiceType), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create service type success', result: req.result })
});

router.patch('/:id', serviceTypeUtil.edit(ServiceType), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'edit service type success', result: req.result })
});

router.delete('/:id', serviceTypeUtil.del(ServiceType), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'delete service type success', result: req.result })
});

module.exports = router;
