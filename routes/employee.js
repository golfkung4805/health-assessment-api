const express = require('express');
const router = express.Router();

const Employee = require("../db/employee");

const validate = require('../controllers/validate');
const employeeUtil = require('../controllers/employee_controller');

router.post('/', employeeUtil.create(Employee), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create employee success' })
});

router.get('/', employeeUtil.getAll(Employee), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create employee success', result: req.result })
});

router.get('/:id', employeeUtil.getById(Employee), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create employee success', result: req.result })
});

router.patch('/:id', employeeUtil.edit(Employee), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'edit employee success', result: req.result })
});

router.delete('/:id', employeeUtil.del(Employee), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'delete employee success', result: req.result })
});

module.exports = router;
