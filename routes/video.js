const express = require('express');
const router = express.Router();

const Video = require("../db/video");

const validate = require('../controllers/validate');
const videoUtil = require('../controllers/video_controller');

router.post('/', videoUtil.create(Video), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create video success' })
});

router.get('/', videoUtil.getAll(Video), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create video success', result: req.result })
});

router.get('/:id', videoUtil.getById(Video), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create video success', result: req.result })
});

router.patch('/:id', videoUtil.edit(Video), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'edit video success', result: req.result })
});

router.delete('/:id', videoUtil.del(Video), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'delete video success', result: req.result })
});

module.exports = router;
