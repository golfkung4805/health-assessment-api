const express = require('express');
const router = express.Router();

const AboutUs = require("../db/aboutUs");

const validate = require('../controllers/validate');
const aboutUsUtil = require('../controllers/aboutUs_controller');

router.post('/', aboutUsUtil.create(AboutUs), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create aboutUsUtil success' })
});

router.get('/', aboutUsUtil.getAll(AboutUs), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create aboutUsUtil success', result: req.result })
});

router.get('/:id', aboutUsUtil.getById(AboutUs), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create aboutUsUtil success', result: req.result })
});

router.patch('/:id', aboutUsUtil.edit(AboutUs), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'edit aboutUsUtil success', result: req.result })
});

router.delete('/:id', aboutUsUtil.del(AboutUs), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'delete aboutUsUtil success', result: req.result })
});

module.exports = router;
