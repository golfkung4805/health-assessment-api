const express = require('express');
const router = express.Router();

const authUtil = require('../controllers/auth_controller')

/* GET home page. */
router.get('/', function(req, res, next) {
  return res.status(200).json({ message: 'Hello world', status: true})
});

router.post('/login', authUtil.login(), (req, res, next) => {
  return res.status(200).json({ message: 'login success', result: req.result, status: true })
})

module.exports = router;
