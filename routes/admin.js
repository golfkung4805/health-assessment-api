const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');

const Admin = require("../db/admin");

const validate = require('../controllers/validate');
const adminUtil = require('../controllers/admin_controller');

router.post('/', adminUtil.create(Admin), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create admin success', result: req.result })
});

router.get('/', adminUtil.getAll(Admin), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create admin success', result: req.result })
});

router.get('/:id', adminUtil.getById(Admin), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'create admin success', result: req.result })
});

router.patch('/:id', adminUtil.edit(Admin), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'edit admin success', result: req.result })
});

router.delete('/:id', adminUtil.del(Admin), async (req, res, next) => {
  return res.status(200).json({ status: true, message: 'delete admin success', result: req.result })
});

module.exports = router;
